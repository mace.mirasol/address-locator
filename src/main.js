import Vue from 'vue';
import App from './App.vue';

import './../node_modules/bulma/css/bulma.min.css';
import './assets/scss/app.scss';

// require('./assets/scss/main.scss');

new Vue({
  el: '#app',
  render: h => h(App)
});
