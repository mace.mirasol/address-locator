const googleMapEmbedLink = 'https://www.google.com/maps/embed/v1/place';
const googleApiKey = 'AIzaSyBDZjCJVC5mbasLkCXqVPBEFE06YSI0Eco';
const place_id = 'ChIJeehHvA7wrjMRe4s9rcHy0LM';

const url = `${googleMapEmbedLink}?key=${googleApiKey}&q=place_id:${place_id}`;

export default url;
