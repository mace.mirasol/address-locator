module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://eslint.org/docs/rules
    'eslint:recommended',
    // https://github.com/vuejs/eslint-plugin-vue#bulb-rules
    'plugin:vue/recommended',
    // https://github.com/prettier/eslint-config-prettier
    'plugin:prettier/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    // https://github.com/vuejs/eslint-plugin-vue
    'vue',
    // https://github.com/prettier/eslint-plugin-prettier
    'prettier'
  ],
  rules: {
    // Only allow `console.log` in development
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // Only allow debugger in development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
};
